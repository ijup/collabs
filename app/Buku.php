<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ['judul','penerbit','kota_terbit','tahun_terbit','deskripsi','gambar','stok','kategori_id' ];
}
