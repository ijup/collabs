@extends('layouts.master')

@section('title')
    Tambah Kategori
@endsection

@section('content')
<h2>Tambah Data Kategori</h2>
<form action="/kategori" method="POST" encytype="multipart/form-data" >
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" id="" >
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection