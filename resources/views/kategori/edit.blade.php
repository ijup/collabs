@extends('layouts.master')

@section('title')
    Edit Kategori {{$kategori->nama}}
@endsection

@section('content')
<h2>Edit Data</h2>
<form action="/kategori/{{$kategori->id}} " method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" value="{{$kategori->nama}}" class="form-control" name="nama" id="" >
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection