@extends('layouts.master')

@section('title')
    Tambah Buku
@endsection

@section('content')
<h2>Tambah Data Buku</h2>
<form action="/buku" method="POST" encytype="multipart/form-data" >
    @csrf
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" id="" >
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="" >
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Kota terbit</label>
        <input type="text" class="form-control" name="kota_terbit" id="" >
        @error('kota_terbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun terbit</label>
        <input type="text" class="form-control" name="tahun_terbit" id="" >
        @error('tahun_terbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <input type="text" class="form-control" name="deskripsi" id="" >
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar</label>
        <input type="file" class="form-control" name="gambar" >
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Stok</label>
        <input type="text" class="form-control" name="stok" id="">
        @error('stok')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="" >
            <option value="">-- Pilih Kategori --</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}} </option>                
            @endforeach
        </select>
        @error('kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection