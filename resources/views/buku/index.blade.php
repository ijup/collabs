@extends('layouts.master')

@section('title')
    List Buku
@endsection

@section('content')

<a href="/buku/create " class="btn btn-primary y-2">Tambah</a>

<div class="row">
    @forelse ($buku as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'.$item->gambar)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5>{{$item->judul}}</h5>
                    <p class="card-text">{{Str::limit($item->deskripsi, 30) }} </p>
                    <a href="/buku/{{$item->id}} " class="btn btn-info btn-sm">Detail</a>
                </div>
            </div>
        </div>
    @empty
        <h1>Tidak ada Buku</h1>
    @endempty
</div>
@endsection